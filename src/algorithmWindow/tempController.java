/**
 * @author Sherine
 * @version 1.0
 * @since 4 December 2016
 */

package algorithmWindow;

import launcher.LauncherController;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

public class tempController extends LauncherController implements Initializable {

	@FXML private Text title;
	
	@FXML private Label ttlHead;
	
	@FXML private Label ttlSeek;
	
	@FXML private Label avgHead;
	
	@FXML private Label avgSeek;
	
	@FXML private LineChart<Integer, Integer> chart;
	@FXML private NumberAxis xAxis;
	@FXML private NumberAxis yAxis;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		title.setText(WindowTitle);
		
		chart.setCreateSymbols(true);
		
		yAxis.setTickLabelsVisible(false);
		yAxis.setMinorTickVisible(false);
		yAxis.setLowerBound(0);
		yAxis.setUpperBound(nRequests);
		
		chart.getData().add(series);
		
		ttlHead.setText(Integer.toString(totalHeadMovement) + " Tracks");
		ttlSeek.setText(Integer.toString(seekTime) + "ms");
		avgHead.setText(Integer.toString(totalHeadMovement/nRequests) + " Tracks");
		avgSeek.setText(Integer.toString(seekTime/nRequests) + "ms");
		
		
		
		
		
	}

}
