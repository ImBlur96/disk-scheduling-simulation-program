/**
 * @author Sherine
 * @version 1.0
 * @since 4 December 2016
 */

package launcher;

import java.io.File;
import algorithms.Algorithm;
import algorithms.CLOOK;
import algorithms.CSCAN;
import algorithms.FCFS;
import algorithms.LOOK;
import algorithms.SCAN;
import algorithms.SSTF;
import fileIO.FileIN;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class LauncherController {

    @FXML private Button dir;

    @FXML private Button fcfs;

    @FXML private Button sstf;

    @FXML private Button scan;

    @FXML private Button cscan;

    @FXML private Button look;

    @FXML private Button clook;
    
    @FXML private TextField filepath;
    
    public static String WindowTitle;
    public static int totalHeadMovement;
    public static int seekTime;
    public static int nRequests;
    public static String selectedPath;
    public static String fileName;
    public static XYChart.Series<Integer, Integer> series;
    
    @FXML
    void fcfsClick(ActionEvent event) {
    	
    	//get input
    	FileIN inputs = new FileIN(selectedPath);
		inputs.getInputs();
		inputs.close();
		
		fileName = inputs.fileName;
		
		//run simulation
		FCFS fcfs1 = new FCFS(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		fcfs1.simulate();
		
		//data
    	totalHeadMovement = fcfs1.getTotalHeadMovement();
    	seekTime = totalHeadMovement*Algorithm.TRACKMOVEMENT_TIME;
    	nRequests = fcfs1.getNumberOfRequests();
    	series = fcfs1.getSeries();
		
    	//display
    	try {
    		WindowTitle = "FCFS - " + fileName;
			Stage window = new Stage();
			Pane root = FXMLLoader.load(getClass().getResource("/algorithmWindow/template.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/algorithmWindow/template.css").toExternalForm());
			
			window.setTitle("First Come First Serve - " + fileName);
			window.setScene(scene);
			window.show();
			window.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
   
    
    @FXML
    void sstfClick(ActionEvent event) {
    	
    	//get inputs
    	FileIN inputs = new FileIN(selectedPath);
		inputs.getInputs();
		inputs.close();
		
		fileName = inputs.fileName;
		
		//run simulation
		SSTF sstf1 = new SSTF(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		sstf1.simulate();
		
		//data
    	totalHeadMovement = sstf1.getTotalHeadMovement();
    	seekTime = totalHeadMovement*Algorithm.TRACKMOVEMENT_TIME;
    	nRequests = sstf1.getNumberOfRequests();
    	series = sstf1.getSeries();
    	
    	//display
    	try {
    		WindowTitle = "SSTF - " + fileName;
			Stage window = new Stage();
			Pane root = FXMLLoader.load(getClass().getResource("/algorithmWindow/template.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/algorithmWindow/template.css").toExternalForm());
			
			window.setTitle("Shortest Seek Time First - " + fileName);
			window.setScene(scene);
			window.show();
			window.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void scanClick(ActionEvent event) {
    	
    	//get inputs
    	FileIN inputs = new FileIN(selectedPath);
		inputs.getInputs();
		inputs.close();
		
		fileName = inputs.fileName;
		
		//run simulation
		SCAN scan1 = new SCAN(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		scan1.simulate();
		
		//data
    	totalHeadMovement = scan1.getTotalHeadMovement();
    	seekTime = totalHeadMovement*Algorithm.TRACKMOVEMENT_TIME;
    	nRequests = scan1.getNumberOfRequests();
    	series = scan1.getSeries();
    	
    	try {
    		WindowTitle = "SCAN - " + fileName;
			Stage window = new Stage();
			Pane root = FXMLLoader.load(getClass().getResource("/algorithmWindow/template.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/algorithmWindow/template.css").toExternalForm());
			
			window.setTitle("SCAN - " + fileName);
			window.setScene(scene);
			window.show();
			window.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void cscanClick(ActionEvent event) {
    	
    	//get inputs
    	FileIN inputs = new FileIN(selectedPath);
		inputs.getInputs();
		inputs.close();
		
		fileName = inputs.fileName;
		
		//run simulation
		CSCAN cscan1 = new CSCAN(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		cscan1.simulate();
		
		//data
    	totalHeadMovement = cscan1.getTotalHeadMovement();
    	seekTime = totalHeadMovement*Algorithm.TRACKMOVEMENT_TIME;
    	nRequests = cscan1.getNumberOfRequests();
    	series = cscan1.getSeries();
    	
    	try {
    		WindowTitle = "CSCAN - " + fileName;
			Stage window = new Stage();
			Pane root = FXMLLoader.load(getClass().getResource("/algorithmWindow/template.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/algorithmWindow/template.css").toExternalForm());
			
			window.setTitle("CSCAN - " + fileName);
			window.setScene(scene);
			window.show();
			window.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void lookClick(ActionEvent event) {
    	
    	//get inputs
    	FileIN inputs = new FileIN(selectedPath);
		inputs.getInputs();
		inputs.close();
		
		fileName = inputs.fileName;
		
		//run simulation
		LOOK look1 = new LOOK(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		look1.simulate();
		
		//data
    	totalHeadMovement = look1.getTotalHeadMovement();
    	seekTime = totalHeadMovement*Algorithm.TRACKMOVEMENT_TIME;
    	nRequests = look1.getNumberOfRequests();
    	series = look1.getSeries();
    	
    	try {
    		WindowTitle = "LOOK - " + fileName;
			Stage window = new Stage();
			Pane root = FXMLLoader.load(getClass().getResource("/algorithmWindow/template.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/algorithmWindow/template.css").toExternalForm());
			
			window.setTitle("LOOK - " + fileName);
			window.setScene(scene);
			window.show();
			window.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void clookClick(ActionEvent event) {
    	
    	//get inputs
    	FileIN inputs = new FileIN(selectedPath);
		inputs.getInputs();
		inputs.close();
		
		fileName = inputs.fileName;
		
		//run simulation
		CLOOK clook1 = new CLOOK(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		clook1.simulate();
		
		//data
    	totalHeadMovement = clook1.getTotalHeadMovement();
    	seekTime = totalHeadMovement*Algorithm.TRACKMOVEMENT_TIME;
    	nRequests = clook1.getNumberOfRequests();
    	series = clook1.getSeries();
    	
    	try {
    		WindowTitle = "CLOOK - " + fileName;
			Stage window = new Stage();
			Pane root = FXMLLoader.load(getClass().getResource("/algorithmWindow/template.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/algorithmWindow/template.css").toExternalForm());
			
			window.setTitle("CLOOK - " + fileName);
			window.setScene(scene);
			window.show();
			window.setResizable(false);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void opendir(ActionEvent event) {
    	try {
    		Stage window = new Stage();
    		FileChooser fileChooser = new FileChooser();
    		fileChooser.setTitle("Select input file");
    		fileChooser.setInitialDirectory(new File("./"));
    		fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("IN", "*.in")
                );
    		File file = fileChooser.showOpenDialog(window);
    		if (file != null){
    			selectedPath = file.getAbsolutePath();
    			filepath.setText(selectedPath);
    		}
    	} catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
}
