/**
 * @author Sherine
 * @version 1.0
 * @since 29 November 2016
 */

package launcher;
	
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

public class Launcher extends Application {	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			
			Pane root = FXMLLoader.load(getClass().getResource("Launcher.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("Launcher.css").toExternalForm());
			
			primaryStage.setTitle("DSSP Main Window");
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setResizable(false);
			
			primaryStage.setOnCloseRequest(e -> Platform.exit());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
