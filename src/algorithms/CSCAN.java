/**
 * @author Zim
 * @version 1.0
 * @since 4th December 2016
 */

package algorithms;

import java.util.ArrayList;
import java.util.Collections;

public class CSCAN extends SCAN {

	public CSCAN(ArrayList<Integer> inputRequests, int hp, int nTracks) {
		super(inputRequests, hp, nTracks);
	}
	
	@Override
	public void simulate(){
		
		System.out.println("Starting CSCAN");
		
		//sort disk locations
		ArrayList<Integer> queue = getrequestBacklog();
		Collections.sort(queue);

		//split list
		int iHeadPosition = queue.indexOf(getHeadPosition());
		queue.remove(iHeadPosition);	
		System.out.println("Sorted requests : " + queue);		
		ArrayList<Integer> downList = new ArrayList<>(queue.subList(0, iHeadPosition));
		ArrayList<Integer> upList = new ArrayList<>(queue.subList(iHeadPosition, queue.size()));
		System.out.println("upList :" + upList);
		System.out.println("downList : " + downList);
		
		int previousPosition = 0;
		int difference = 0;
		//simulate according to order
		if(getHeadPosition() > getNumberOfTracks()/2){
			//up first then reset and go up
			System.out.println("Going UP");
			simulateUp(upList);
			
			//move to the top
			previousPosition = getHeadPosition();
			setHeadPosition(getNumberOfTracks()-1);
			difference = Math.abs(getHeadPosition()-previousPosition);
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			//reset
			previousPosition = getHeadPosition();
			setHeadPosition(0);
			
			System.out.println("Reset and going UP");
			simulateUp(downList);					
			
		}
		else{
			//down first then reset and go down
			System.out.println("Going DOWN");
			simulateDown(downList);
			
			//move to the bottom
			previousPosition = getHeadPosition();
			setHeadPosition(0);
			difference = Math.abs(getHeadPosition()-previousPosition);
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			//reset
			previousPosition = getHeadPosition();
			setHeadPosition(getNumberOfTracks()-1);
			
			System.out.println("Reset and going DOWN");
			simulateDown(upList);
			
		}
		
		System.out.println("Done CSCAN");
		
	}	

}
