/**
 * @author Zim
 * @version 1.0
 * @since 3rd December 2016
 */

package algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SCAN extends Algorithm {

	public SCAN(ArrayList<Integer> inputRequests, int hp, int nTracks) {
		super(inputRequests, hp, nTracks);
		getrequestBacklog().add(getHeadPosition());
	}

	@Override
	public void simulate() {
		
		System.out.println("Starting SCAN");
		
		//sort disk locations
		ArrayList<Integer> queue = getrequestBacklog();
		Collections.sort(queue);

		//split list
		int iHeadPosition = queue.indexOf(getHeadPosition());
		queue.remove(iHeadPosition);	
		System.out.println("Sorted requests : " + queue);		
		ArrayList<Integer> downList = new ArrayList<>(queue.subList(0, iHeadPosition));
		ArrayList<Integer> upList = new ArrayList<>(queue.subList(iHeadPosition, queue.size()));
		System.out.println("upList :" + upList);
		System.out.println("downList : " + downList);
		
		int previousPosition = 0;
		int difference = 0;
		//simulate according to order
		if(getHeadPosition() > getNumberOfTracks()/2){
			//up first then down
			System.out.println("Going UP");
			simulateUp(upList);
			
			previousPosition = getHeadPosition();
			setHeadPosition(getNumberOfTracks()-1);
			difference = Math.abs(previousPosition-getHeadPosition());
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			System.out.println("Going DOWN");
			simulateDown(downList);					
			
		}
		else{
			//down first then up
			System.out.println("Going DOWN");
			simulateDown(downList);
			
			previousPosition = getHeadPosition();
			setHeadPosition(0);
			difference = Math.abs(previousPosition-getHeadPosition());
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			System.out.println("Going UP");
			simulateUp(upList);
			
		}
		
		System.out.println("Done SCAN");
			
	}
	
	protected void simulateUp(List<Integer> list){
		
		while(list.size() > 0){
			int previousPosition = getHeadPosition();
			setHeadPosition(list.get(0));
			list.remove(0);
			
			int difference = Math.abs(previousPosition-getHeadPosition());
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			System.out.println("THM = " + getTotalHeadMovement());
			
		}
		
	}
	
	protected void simulateDown(List<Integer> list){
		
		while(list.size() > 0){
			int previousPosition = getHeadPosition();
			setHeadPosition(list.get(list.size()-1));
			list.remove(list.size()-1);
			
			int difference = Math.abs(previousPosition-getHeadPosition());
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			System.out.println("THM = " + getTotalHeadMovement());
			
		}
		
	}

}
