package algorithms;

import java.util.ArrayList;

/**
 * @author Zim
 * @version 1.0
 * @since 30th November 2016
 *
 */
public class FCFS extends Algorithm {
	
	public FCFS(ArrayList<Integer> inputRequests, int hp, int nTracks) {
		super(inputRequests, hp, nTracks);
	}
	
	@Override
	public void simulate() {
		
		System.out.println("Starting FCFS");
		
		ArrayList<Integer> queue = getrequestBacklog();
		while(queue.size() > 0){
			int previousPosition = getHeadPosition();
			setHeadPosition(queue.get(0));
			queue.remove(0);
			
			int difference = Math.abs(previousPosition-getHeadPosition());
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			System.out.println("THM = " + getTotalHeadMovement());
		}
		System.out.println("Done FCFS");
	}

}
