/**
 * @author Zim
 * @version 1.0
 * @since 4th December 2016
 */

package algorithms;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.junit.Test;

import fileIO.FileIN;

public class testAlgorithms {

	@Test
	public void testFCFS() throws FileNotFoundException {
		//Run Algorithm
		FileIN inputs = new FileIN("Resources/testCases/testCase1.in");
		inputs.getInputs();
		inputs.close();
		FCFS fcfs1 = new FCFS(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		fcfs1.simulate();
		
		//Compare with expected output
		FileReader expected = new FileReader("Resources/testCases/testCase1FCFSExpected.txt");
		BufferedReader expectedBuff = new BufferedReader(expected);
		Scanner in = new Scanner(expectedBuff);
		
		int nRequests = fcfs1.getNumberOfRequests();
		int totalHeadMovement = fcfs1.getTotalHeadMovement();
		Double avgHeadMovement = (double) totalHeadMovement/nRequests;
		int totalSeekTime = totalHeadMovement * Algorithm.TRACKMOVEMENT_TIME;
		Double avgSeekTime = (double) totalSeekTime/nRequests;
		
		assertEquals(in.nextInt(), totalHeadMovement);
		assertEquals(new Double(in.nextDouble()), avgHeadMovement);
		assertEquals(in.nextInt(), totalSeekTime);
		assertEquals(new Double(in.nextDouble()), avgSeekTime);
		
		try {
			in.close();
			expectedBuff.close();
			expected.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testSSTF() throws FileNotFoundException {
		//Run Algorithm
		FileIN inputs = new FileIN("Resources/testCases/testCase1.in");
		inputs.getInputs();
		inputs.close();
		SSTF sstf1 = new SSTF(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		sstf1.simulate();
		
		//Compare with expected output
		FileReader expected = new FileReader("Resources/testCases/testCase1SSTFExpected.txt");
		BufferedReader expectedBuff = new BufferedReader(expected);
		Scanner in = new Scanner(expectedBuff);
		
		int nRequests = sstf1.getNumberOfRequests();
		int totalHeadMovement = sstf1.getTotalHeadMovement();
		Double avgHeadMovement = (double) totalHeadMovement/nRequests;
		int totalSeekTime = totalHeadMovement * Algorithm.TRACKMOVEMENT_TIME;
		Double avgSeekTime = (double) totalSeekTime/nRequests;
		
		assertEquals(in.nextInt(), totalHeadMovement);
		assertEquals(new Double(in.nextDouble()), avgHeadMovement);
		assertEquals(in.nextInt(), totalSeekTime);
		assertEquals(new Double(in.nextDouble()), avgSeekTime);
		
		try {
			in.close();
			expectedBuff.close();
			expected.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSCAN() throws FileNotFoundException {
		//Run Algorithm
		FileIN inputs = new FileIN("Resources/testCases/testCase1.in");
		inputs.getInputs();
		inputs.close();
		SCAN scan1 = new SCAN(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		scan1.simulate();
		
		//Compare with expected output
		FileReader expected = new FileReader("Resources/testCases/testCase1SCANExpected.txt");
		BufferedReader expectedBuff = new BufferedReader(expected);
		Scanner in = new Scanner(expectedBuff);
		
		int nRequests = scan1.getNumberOfRequests();
		int totalHeadMovement = scan1.getTotalHeadMovement();
		Double avgHeadMovement = (double) totalHeadMovement/nRequests;
		int totalSeekTime = totalHeadMovement * Algorithm.TRACKMOVEMENT_TIME;
		Double avgSeekTime = (double) totalSeekTime/nRequests;
		
		assertEquals(in.nextInt(), totalHeadMovement);
		assertEquals(new Double(in.nextDouble()), avgHeadMovement);
		assertEquals(in.nextInt(), totalSeekTime);
		assertEquals(new Double(in.nextDouble()), avgSeekTime);
		
		try {
			in.close();
			expectedBuff.close();
			expected.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testCSCAN() throws FileNotFoundException {
		//Run Algorithm
		FileIN inputs = new FileIN("Resources/testCases/testCase1.in");
		inputs.getInputs();
		inputs.close();
		CSCAN cscan1 = new CSCAN(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		cscan1.simulate();
		
		//Compare with expected output
		FileReader expected = new FileReader("Resources/testCases/testCase1CSCANExpected.txt");
		BufferedReader expectedBuff = new BufferedReader(expected);
		Scanner in = new Scanner(expectedBuff);
		
		int nRequests = cscan1.getNumberOfRequests();
		int totalHeadMovement = cscan1.getTotalHeadMovement();
		Double avgHeadMovement = (double) totalHeadMovement/nRequests;
		int totalSeekTime = totalHeadMovement * Algorithm.TRACKMOVEMENT_TIME;
		Double avgSeekTime = (double) totalSeekTime/nRequests;
		
		assertEquals(in.nextInt(), totalHeadMovement);
		assertEquals(new Double(in.nextDouble()), avgHeadMovement);
		assertEquals(in.nextInt(), totalSeekTime);
		assertEquals(new Double(in.nextDouble()), avgSeekTime);
		
		try {
			in.close();
			expectedBuff.close();
			expected.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testLOOK() throws FileNotFoundException {
		//Run Algorithm
		FileIN inputs = new FileIN("Resources/testCases/testCase1.in");
		inputs.getInputs();
		inputs.close();
		LOOK look1 = new LOOK(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		look1.simulate();
		
		//Compare with expected output
		FileReader expected = new FileReader("Resources/testCases/testCase1LOOKExpected.txt");
		BufferedReader expectedBuff = new BufferedReader(expected);
		Scanner in = new Scanner(expectedBuff);
		
		int nRequests = look1.getNumberOfRequests();
		int totalHeadMovement = look1.getTotalHeadMovement();
		Double avgHeadMovement = (double) totalHeadMovement/nRequests;
		int totalSeekTime = totalHeadMovement * Algorithm.TRACKMOVEMENT_TIME;
		Double avgSeekTime = (double) totalSeekTime/nRequests;
		
		assertEquals(in.nextInt(), totalHeadMovement);
		assertEquals(new Double(in.nextDouble()), avgHeadMovement);
		assertEquals(in.nextInt(), totalSeekTime);
		assertEquals(new Double(in.nextDouble()), avgSeekTime);
		
		try {
			in.close();
			expectedBuff.close();
			expected.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testCLOOK() throws FileNotFoundException {
		//Run Algorithm
		FileIN inputs = new FileIN("Resources/testCases/testCase1.in");
		inputs.getInputs();
		inputs.close();
		CLOOK clook1 = new CLOOK(inputs.getReqBacklog(), inputs.getHeadPos(), inputs.getNumberOfTracks());
		clook1.simulate();
		
		//Compare with expected output
		FileReader expected = new FileReader("Resources/testCases/testCase1CLOOKExpected.txt");
		BufferedReader expectedBuff = new BufferedReader(expected);
		Scanner in = new Scanner(expectedBuff);
		
		int nRequests = clook1.getNumberOfRequests();
		int totalHeadMovement = clook1.getTotalHeadMovement();
		Double avgHeadMovement = (double) totalHeadMovement/nRequests;
		int totalSeekTime = totalHeadMovement * Algorithm.TRACKMOVEMENT_TIME;
		Double avgSeekTime = (double) totalSeekTime/nRequests;
		
		assertEquals(in.nextInt(), totalHeadMovement);
		assertEquals(new Double(in.nextDouble()), avgHeadMovement);
		assertEquals(in.nextInt(), totalSeekTime);
		assertEquals(new Double(in.nextDouble()), avgSeekTime);
		
		try {
			in.close();
			expectedBuff.close();
			expected.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
