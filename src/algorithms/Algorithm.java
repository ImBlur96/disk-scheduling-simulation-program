package algorithms;

/**
 * @author Zim
 * @version 1.0
 * @since 29 November 2016
 */

import java.util.ArrayList;

import javafx.scene.chart.XYChart;

public abstract class Algorithm {
	
	//Time for one track movement in milliseconds
	public static final int TRACKMOVEMENT_TIME = 5;
	
	private int numberOfTracks;
	private int headPosition;
	private int numberOfRequests;
	private ArrayList<Integer> requestBacklog;
	private int totalHeadMovement;
	
	private XYChart.Series<Integer, Integer> series;
	private int counter;

	//Abstract methods to be implemented by individual algorithms
	public abstract void simulate();
	
	public Algorithm(ArrayList<Integer> inputRequests, int hp, int nTracks){		
		series = new XYChart.Series<>();
		series.setName("Head Movement");
		requestBacklog = inputRequests;
		counter = numberOfRequests;
		
		setHeadPosition(hp);	
		setTotalHeadMovement(0);		
		setNumberOfTracks(nTracks);
		setNumberOfRequests(inputRequests.size());		
	}
	
	public ArrayList<Integer> getrequestBacklog(){		
		return requestBacklog;		
	}

	public int getHeadPosition() {
		return headPosition;
	}

	public void setHeadPosition(Integer headPosition) {
		series.getData().add(new XYChart.Data<>(headPosition, --counter));
		this.headPosition = headPosition;
	}

	public int getTotalHeadMovement() {
		return totalHeadMovement;
	}

	public void setTotalHeadMovement(Integer seekTime) {
		this.totalHeadMovement = seekTime;
	}

	public int getNumberOfTracks() {
		return numberOfTracks;
	}

	public void setNumberOfTracks(int numberOfTracks) {
		this.numberOfTracks = numberOfTracks;
	}

	public int getNumberOfRequests() {
		return numberOfRequests;
	}

	public void setNumberOfRequests(int numberOfRequests) {
		this.numberOfRequests = numberOfRequests;
	}
	
	public XYChart.Series<Integer, Integer> getSeries() {
		return series;
	}
	
}
