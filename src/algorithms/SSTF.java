/**
 * @author Zim
 * @version 1.0
 * @since 3rd December 2016
 */

package algorithms;

import java.util.ArrayList;

public class SSTF extends Algorithm {

	public SSTF(ArrayList<Integer> inputRequests, int hp, int nTracks) {
		super(inputRequests, hp, nTracks);
	}
	
	@Override
	public void simulate() {
		
		System.out.println("Starting SSTF");
		
		ArrayList<Integer> queue = getrequestBacklog();
		while(queue.size() > 0){
			int previousPosition = getHeadPosition();
			
			//find shortest seek time
			int shortestTime = getNumberOfTracks()+1;
			int iShortestTime = 0;
			for(int i = 0; i < queue.size(); i++){
				int tempDiff = Math.abs(previousPosition - queue.get(i));
				if(tempDiff < shortestTime){
					shortestTime = tempDiff;
					iShortestTime = i;
				}
			}
			setHeadPosition(queue.get(iShortestTime));
			queue.remove(iShortestTime);
			
			int difference = Math.abs(previousPosition-getHeadPosition());
			setTotalHeadMovement(getTotalHeadMovement()+difference);
			
			System.out.println("THM = " + getTotalHeadMovement());
		}
		System.out.println("Done SSTF");
	}
}
