/**
 * @author Zim
 * @version 1.0
 * @since 4th December 2016
 */

package algorithms;

import java.util.ArrayList;
import java.util.Collections;

public class CLOOK extends LOOK {

	public CLOOK(ArrayList<Integer> inputRequests, int hp, int nTracks) {
		super(inputRequests, hp, nTracks);
	}

	@Override
	public void simulate() {
		
		System.out.println("Starting CLOOK");
		
		//sort disk locations
		ArrayList<Integer> queue = getrequestBacklog();
		Collections.sort(queue);

		//split list
		int iHeadPosition = queue.indexOf(getHeadPosition());
		queue.remove(iHeadPosition);	
		System.out.println("Sorted requests : " + queue);		
		ArrayList<Integer> downList = new ArrayList<>(queue.subList(0, iHeadPosition));
		ArrayList<Integer> upList = new ArrayList<>(queue.subList(iHeadPosition, queue.size()));
		System.out.println("upList :" + upList);
		System.out.println("downList : " + downList);
		
		//simulate according to order
		if(getHeadPosition() > getNumberOfTracks()/2){
			//up first then down
			System.out.println("Going UP");
			simulateUp(upList);
			
			setHeadPosition(downList.get(0));
			
			System.out.println("Reset and Going UP");
			simulateUp(downList);					
			
		}
		else{
			//down first then up
			System.out.println("Going DOWN");
			simulateDown(downList);
			
			setHeadPosition(upList.get(upList.size()-1));
			
			System.out.println("Reset and Going DOWN");
			simulateDown(upList);
			
		}
		
		System.out.println("Done CLOOK");
			
	}
	
}
