/**
 * @author Zim
 * @version 1.0
 * @since 29 November 2016
 */

package fileIO;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;

public class testFileIO {
	
	public static String head = "145";
	public static String nTracks = "200";
	public static ArrayList<Integer> requests = new ArrayList<>(Arrays.asList(15, 25, 30, 50, 5, 150));

	@Test
	public void startTestFileIO() {
		
		//Write to file
		FileOUT.init("testFileIO.out");
		FileOUT.writeLine("#This is a Comment");
		FileOUT.writeLine("#Next Value is Head Position");
		FileOUT.writeLine(head);
		FileOUT.writeLine("#Next Value is number of Tracks");
		FileOUT.writeLine("");
		FileOUT.writeLine(nTracks);
		FileOUT.writeLine("#Next Values are requests");
		for(Integer each : requests){
			FileOUT.writeLine(Integer.toString(each));
		}
		FileOUT.close();
		
		//Read and confirm
		FileIN inputs = new FileIN("testFileIO.out");
		inputs.getInputs();
		assertEquals(Integer.parseInt(head), inputs.getHeadPos());
		assertEquals(requests, inputs.getReqBacklog());
		assertEquals(Integer.parseInt(nTracks), inputs.getNumberOfTracks());
		inputs.close();
	}
	
}
