package fileIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * @author Zim
 * @version 1.0
 * @since 29 November 2016
 */

public class FileIN {

	private FileReader inFileReader;
	private BufferedReader inBuffReader;
	private Scanner in;
	
	private File file;
	public String fileName;
	
	private int headPos;
	private int numberOfTracks;
	private ArrayList<Integer> reqBacklog;
	
	public FileIN(String dir){
		
		try {
			file = new File(dir); 
			fileName = file.getName();
			
			inFileReader = new FileReader(file);
			inBuffReader = new BufferedReader(inFileReader);
			in = new Scanner(inBuffReader);
		} catch (FileNotFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("DSSP ERROR");
			alert.setHeaderText("INVALID FILE");
			alert.setContentText("File specified is either doesn't exist or unreadable");

			alert.showAndWait();
		} catch (NullPointerException np){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("DSSP ERROR");
			alert.setHeaderText("NO INPUT FILE SELECTED");
			alert.setContentText("Please specify a path to an input file");

			alert.showAndWait();
		}
		
		reqBacklog = new ArrayList<>();
		
	}
	
	//Retrieve all data from File and store into static variables
	public void getInputs(){
		
		boolean flag_HeadPos = false;
		boolean flag_nTracks = false;
		
		try {
			while(in.hasNext()){			
				String curLine = in.nextLine();
				//Ignore comments and empty lines
				if(curLine.indexOf('#') == -1 && curLine.compareTo("") != 0){
					if(!flag_HeadPos){
						setHeadPos(Integer.parseInt(curLine));
						flag_HeadPos = true;
					}
					else if(!flag_nTracks){
						setNumberOfTracks(Integer.parseInt(curLine));
						flag_nTracks = true;
					}
					else{
						reqBacklog.add(Integer.parseInt(curLine));
					}
				}
			}
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("DSSP ERROR");
			alert.setHeaderText("Error while reading from file");
			alert.setContentText("Please check the structure of the input file");

			alert.showAndWait();
		}
		
	}
	
	public void close(){
		
		try {
			in.close();
			inBuffReader.close();
			inFileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public int getHeadPos() {
		return headPos;
	}

	public void setHeadPos(Integer headPos) {
		this.headPos = headPos;
	}
	
	public ArrayList<Integer> getreqBacklog(){
		return reqBacklog;
	}

	public ArrayList<Integer> getReqBacklog(){
		return reqBacklog;
	}

	public int getNumberOfTracks() {
		return numberOfTracks;
	}

	private void setNumberOfTracks(int numberOfTracks) {
		this.numberOfTracks = numberOfTracks;
	}
	
}
