package fileIO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Zim
 * @version 0.2
 * @since 29 November 2016
 */

public class FileOUT {
	
	private static FileWriter outputFile = null;
	private static BufferedWriter outBuffWriter = null;
	
	public static void init(String dir){
		
		try {
			outputFile = new FileWriter(dir);
			outBuffWriter = new BufferedWriter(outputFile);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void writeLine(String toBeWritten){
		
		try {
			outBuffWriter.write(toBeWritten);
			outBuffWriter.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void close(){
		
		try {
			outBuffWriter.close();
			outputFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
